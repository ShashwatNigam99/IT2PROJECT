﻿# Taskmanager205
Flask based taskmanager app 

![Screenshot_from_2018-05-02_22-09-41](/uploads/5e693e2e027f69e2210262f5a5fa97d7/Screenshot_from_2018-05-02_22-09-41.png)

### Instructions to run :<br>
1. Clone the repo and cd into it.<br>
2. Create a virtualenv<br>
```virtualenv venv --python=python2.7```<br>
3. Activate the virtualenv<br>
```source venv/bin/activate```<br>
4. Install requirements<br>
```pip install -r requirements.txt```<br>
5. Set the environment variable for the flask app<br>
```export FLASK_APP=taskmanager.py```
5. Run the server by running the following command<br>
```flask run```
